"""Mandrill target sink class, which handles writing streams."""


import mailchimp_transactional as MailchimpTransactional
from mailchimp_transactional.api_client import ApiClientError
from singer_sdk.sinks import BatchSink


class MandrillSink(BatchSink):
    """Mailchimp target sink class."""

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""

        mailchimp = MailchimpTransactional.Client(self.config['api_key'])
        messages = context['records']
        for message in messages:
            try:
                response = mailchimp.messages.send({"message":message})
                self.logger.debug('API called successfully: {}'.format(response))
            except ApiClientError as error:
                self.logger.error('An exception occurred: {}'.format(error.text))

