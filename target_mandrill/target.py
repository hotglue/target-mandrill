"""Mandrill target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_mandrill.sinks import MandrillSink


class TargetMandrill(Target):
    """Sample target for Mandrill."""

    name = "target-mandrill"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True
        ),
    ).to_dict()

    default_sink_class = MandrillSink


if __name__ == '__main__':
    TargetMandrill.cli()